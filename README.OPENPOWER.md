Requirements
========================

An OpenPOWER (e.g. Talos II, Blackbird, Sparrowhawk, etc.) system running FreeBSD 13.2

How to build
=========================

Clone repositories
=========================
    # cd /usr
    # git clone https://gitlab.raptorengineering.com/raptor-engineering-public/opnsense/core
    # git clone https://gitlab.raptorengineering.com/raptor-engineering-public/opnsense/src
    # git clone https://gitlab.raptorengineering.com/raptor-engineering-public/opnsense/plugins
    # git clone https://gitlab.raptorengineering.com/raptor-engineering-public/opnsense/ports
    # git clone https://gitlab.raptorengineering.com/raptor-engineering-public/opnsense/tools

Check out the required source versions
=========================
    # cd /usr/src
    # git checkout stable/24.1

    # cd /usr/ports
    # git checkout stable/24.1.x

    # cd /usr/plugins
    # git checkout stable/24.1

    # cd /usr/core
    # git checkout stable/24.1

Configure repository signing keys
=========================
    # cd /usr/tools
    # openssl genrsa -out /usr/tools/config/24.1/repo.key 4096
    # openssl rsa -pubout -in /usr/tools/config/24.1/repo.key -out /usr/tools/config/24.1/repo.pub
    # make fingerprint DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x

Build
=========================

Note: The initial ports build will take several days on a smaller 4-core box
A large multi-core system (at least 16 cores) is highly recommended

    # cd /usr/tools
    # make base DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make kernel DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make ports DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make plugins DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make core DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make openpower DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x
    # make sets DEVICE=OPENPOWER PORTSBRANCH=stable/24.1.x

Deploy
=========================

Image files are located in the following directory
/usr/local/opnsense/build/24.1/powerpc64le/images/

Package files are located in the archive created here:
/usr/local/opnsense/build/24.1/powerpc64le/sets/packages-24.1.11_2-OpenSSL-powerpc64le.tar
